using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using NUnit.Framework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using program.Controllers;
using program.Models;

namespace program.Tests
{
    [TestFixture, Category("usability")]
    public class programUsabilityTest
    {
        DefaultHttpContext httpContext;
        [SetUp]
        public void Setup()
        {
            //If you manually debugging, run Payment service and set the url below
            //Environment.SetEnvironmentVariable("paymenturl", "http://172.17.0.1:5001/api/payment");
            httpContext = new DefaultHttpContext();
            httpContext.Request.Scheme = "http";
            httpContext.Request.Host = new HostString("localhost");
        }

        [Test]
        public async Task should_allow_transfer_action()
        {
            //Arrange
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", "transfer" },
                { "amount", "100" }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };


            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "transfer",
                Amount = 100
            };

            //Act
            var result = await controller.PostActionItem(expectedActionItem);

            //Assert
            Assert.That(result, Is.InstanceOf<Tuple<ActionItem, string>>());
            Assert.That(result.Item1, Is.Not.Null);
            Assert.That(result.Item2, Is.Not.Null);
            Assert.AreEqual(result.Item1.Action, expectedActionItem.Action);
            Assert.AreEqual(result.Item2, "Successfully transfered $100");

        }
        [Test]
        public async Task should_disallow_withdraw_action()
        {
            //Arrange
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", "withdraw" },
                { "amount", "100" }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };


            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "withdraw",
                Amount = 100
            };

            //Act
            var result = await controller.PostActionItem(expectedActionItem);

            //Assert
            Assert.That(result, Is.InstanceOf<Tuple<ActionItem, string>>());
            Assert.That(result.Item1.Action, Is.Null);
            Assert.That(result.Item2, Is.Not.Null);
            Assert.AreEqual(result.Item2, "You can only transfer an amount");

        }
    }

}
